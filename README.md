# TP Spring/Angular JS Agile (TAA/GLI)

## Etudiants

Cette application est crée par Ouhammouch Salma & Assal Mohamed Rida 
Etudiants en Master2 MITIC

## Repertoire

Ce repertoire contient l'application TAA  Jax-RS 

## Lancement

# Jax-RS

Pour lancer l'application Jax-RS il faut lancer la class "RestApplication" qui se trouve dans le package src/main/java/app

## Accès au service web

Effectuer les requêtes HTTP à l'URL de base en utilisant Swagger (http://localhost:8080/api) ou  http://localhost:8080/
Par exemple


 Type  |              URL             |                                        Action
-------|------------------------------|------------------------------------------------------------------------
  GET  |  /projects/findall           | Retourne la liste de tous les projects
  GET  |  /developers/findall         | Retourne la liste de tous les developers
  GET  |  /sprints/findall            | Retourne la liste de tous les sprints
  GET  |  /tasks/findall              | Retourne la liste de tous les tasks