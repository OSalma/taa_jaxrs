package dao;

import java.io.Serializable;
import java.util.List;

public interface AgileDao<T, Id extends Serializable> {

	public void insert(T entity);

	public void update(T entity);

	public List<T> findAll();

	public T findById(int id);

	public void delete(int id);

	public void deleteAll();
}
