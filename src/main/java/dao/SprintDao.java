/**
 * 
 */
package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import manager.EntityManagerHelper;
import domain.Sprint;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class SprintDao implements AgileDao<Sprint, Serializable> {
	
	private static final String SELECT_QUERY = "select s from Sprint s";

	private EntityManager manager;

	/**
	 * 
	 */
	public SprintDao() {
		super();
		this.manager = EntityManagerHelper.getEntityManager();
	}

	@Override
	public void insert(Sprint entity) {
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			manager.persist(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
	}

	@Override
	public void update(Sprint entity) {
		manager.getTransaction().begin();
		manager.merge(entity);
		manager.getTransaction().commit();		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Sprint> findAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Sprint> sprints = (List<Sprint>) query.getResultList();
		return sprints;
	}

	@Override
	public Sprint findById(int id) {
		return manager.find(Sprint.class, id);
	}

	@Override
	public void delete(int id) {
		manager.getTransaction().begin();
		manager.remove(manager.find(Sprint.class, id));
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Sprint> sprints = (List<Sprint>) query.getResultList();
		for (Sprint s : sprints) {
			manager.getTransaction().begin();
			manager.remove(s);
			manager.getTransaction().commit();
		}
	}

}
