/**
 * 
 */
package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import domain.Developer;
import domain.Project;
import manager.EntityManagerHelper;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 * 
 */
public class ProjectDao implements AgileDao<Project, Serializable> {

	private static final String SELECT_QUERY = "select p from Project p";

	private EntityManager manager;

	public ProjectDao() {
		super();
		this.manager = EntityManagerHelper.getEntityManager();
	}

	@Override
	public void insert(Project entity) {
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			manager.persist(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
	}

	@Override
	public void update(Project entity) {
		manager.getTransaction().begin();
		manager.merge(entity);
		manager.getTransaction().commit();
	}

	@Override
	public List<Project> findAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Project> projects = (List<Project>) query.getResultList();
		return projects;
	}

	@Override
	public Project findById(int id) {
		return manager.find(Project.class, id);
	}

	@Override
	public void delete(int id) {
		manager.getTransaction().begin();
		manager.remove(manager.find(Project.class, id));
		manager.getTransaction().commit();
	}

	@Override
	public void deleteAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Project> projects = (List<Project>) query.getResultList();
		for (Project d : projects) {
			manager.getTransaction().begin();
			manager.remove(d);
			manager.getTransaction().commit();
		}
	}

}
