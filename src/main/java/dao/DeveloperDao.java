package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import manager.EntityManagerHelper;
import domain.Developer;

public class DeveloperDao implements AgileDao<Developer, Serializable> {

	private static final String SELECT_QUERY = "select d from Developer d";

	private EntityManager manager;

	public DeveloperDao() {
		super();
		this.manager = EntityManagerHelper.getEntityManager();
	}

	@Override
	public void insert(Developer entity) {
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			manager.persist(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
	}

	@Override
	public void update(Developer entity) {
		manager.getTransaction().begin();
		manager.merge(entity);
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Developer> findAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Developer> developers = (List<Developer>) query.getResultList();
		return developers;
	}

	@Override
	public Developer findById(int id) {
		return manager.find(Developer.class, id);
	}

	@Override
	public void delete(int id) {
		manager.getTransaction().begin();
		manager.remove(manager.find(Developer.class, id));
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Developer> developers = (List<Developer>) query.getResultList();
		for (Developer d : developers) {
			manager.getTransaction().begin();
			manager.remove(d);
			manager.getTransaction().commit();
		}
	}

}
