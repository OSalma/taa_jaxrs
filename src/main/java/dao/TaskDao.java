/**
 * 
 */
package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import manager.EntityManagerHelper;
import domain.Task;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class TaskDao implements AgileDao<Task, Serializable> {
	
	private static final String SELECT_QUERY = "select t from Task t";

	private EntityManager manager;

	/**
	 * 
	 */
	public TaskDao() {
		super();
		this.manager = EntityManagerHelper.getEntityManager();
	}

	@Override
	public void insert(Task entity) {
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			manager.persist(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
	}

	@Override
	public void update(Task entity) {
		manager.getTransaction().begin();
		manager.merge(entity);
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> findAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Task> tasks = (List<Task>) query.getResultList();
		return tasks;
	}

	@Override
	public Task findById(int id) {
		return manager.find(Task.class, id);
	}

	@Override
	public void delete(int id) {
		manager.getTransaction().begin();
		manager.remove(manager.find(Task.class, id));
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteAll() {
		Query query = manager.createQuery(SELECT_QUERY);
		List<Task> tasks = (List<Task>) query.getResultList();
		for (Task d : tasks) {
			manager.getTransaction().begin();
			manager.remove(d);
			manager.getTransaction().commit();
		}
	}

}
