package manager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class ElementManager<ElementType> {
	private EntityManagerFactory factory;
	private EntityManager manager;

	private void startManager() {
		this.factory = Persistence.createEntityManagerFactory("dev");
		this.manager = factory.createEntityManager();
	}

	private void destroyManager() {
		this.manager.close();
		this.factory.close();
		this.manager = null;
		this.factory = null;
	}

	public EntityManager getManager() {
		startManager();
		return manager;
	}

	public void closeManager() {
		destroyManager();
	}

	public List<ElementType> getElements(String tableName) {
		startManager();
		List<ElementType> elements = manager.createQuery(
				"SELECT e FROM " + tableName + " e").getResultList();
		// destroyManager();
		return elements;
	}

	public ElementType getElement(Class<ElementType> tClass, Object id) {
		startManager();
		ElementType element = manager.find(tClass, id);
		// destroyManager();
		return element;
	}

	public ElementType addElement(ElementType element) {
		startManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		manager.persist(element);
		transaction.commit();
		// destroyManager();
		return element;
	}

	public ElementType updateElement(ElementType element) {
		startManager();
		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();
		manager.merge(element);
		transaction.commit();
		// / destroyManager();
		return element;
	}

	public ElementType deleteElement(Class<ElementType> tClass, Object id) {
		startManager();
		ElementType element = manager.find(tClass, id);
		if (element != null) {
			EntityTransaction transaction = manager.getTransaction();
			transaction.begin();
			manager.remove(element);
			transaction.commit();
		}
		// destroyManager();
		return element;
	}

}
