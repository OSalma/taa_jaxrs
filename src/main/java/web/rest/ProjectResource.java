/**
 * 
 */
package web.rest;

import io.swagger.annotations.Api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.ProjectDao;
import domain.Project;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 * 
 */
@Path("/project")
@Api(value = "/project", description = "Project resource")
public class ProjectResource {

	private ProjectDao projectDao;

	public ProjectResource() {
		this.projectDao = new ProjectDao();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProject(Project d) {
		this.projectDao.insert(d);
		return Response.ok("insert").build();
	}

	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProject(Project d) {
		this.projectDao.update(d);
		return Response.ok("update").build();
	}

	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Project> getAllProject() {
		return this.projectDao.findAll();
	}

	@GET
	@Path("/find/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Project getProjectById(@PathParam("id") int id) {
		return this.projectDao.findById(id);
	}

	@DELETE
	@Path("/delete/{id}")
	public void deleteDevolper(@PathParam("id") int id) {
		this.projectDao.delete(id);
	}

	@DELETE
	@Path("/deleteall")
	public Response deleteAllProject() {
		this.projectDao.deleteAll();
		return Response.ok("deleteAll").build();
	}
}
