/**
 * 
 */
package web.rest;

import java.util.Collection;

import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.SprintDao;
import domain.Sprint;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
@Path("/sprint")
@Api(value = "/sprint", description = "Sprint resource")
public class SprintResource {
	
	private SprintDao sprintDao;

	/**
	 * 
	 */
	public SprintResource() {
		this.sprintDao = new SprintDao();
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addSprint(Sprint s) {
		this.sprintDao.insert(s);
		return Response.ok("insert").build();
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSprint(Sprint s) {
		this.sprintDao.update(s);
		return Response.ok("update").build();
	}
	
	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Sprint> getAllSprint() {
		return this.sprintDao.findAll();
	}
	
	@GET
	@Path("/find/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Sprint getDeveloperById(@PathParam("id") int id) {
		return this.sprintDao.findById(id);
	}
	
	@DELETE
	@Path("/delete/{id}")
	public void deleteDevolper(@PathParam("id") int id) {
		this.sprintDao.delete(id);
	}
	
	@DELETE
	@Path("/deleteall")
	public Response deleteAllDevolper() {
		this.sprintDao.deleteAll();
		return Response.ok("deleteAll").build();
	}

}
