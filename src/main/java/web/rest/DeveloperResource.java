package web.rest;

import io.swagger.annotations.Api;

import java.util.List;

import domain.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

import dao.DeveloperDao;

@Path("/developer")
@Api(value = "/developer", description = "Developer resource")
public class DeveloperResource {

	private DeveloperDao developerDao;

	public DeveloperResource() {
		this.developerDao = new DeveloperDao();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDeveloper(Developer d) {
		this.developerDao.insert(d);
		return Response.ok("insert").build();
	}

	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDeveloper(Developer d) {
		this.developerDao.update(d);
		return Response.ok("update").build();
	}

	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Developer> getAllDeveloper() {
		return this.developerDao.findAll();
	}

	@GET
	@Path("/find/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Developer getDeveloperById(@PathParam("id") int id) {
		return this.developerDao.findById(id);
	}

	@DELETE
	@Path("/delete/{id}")
	public void deleteDevolper(@PathParam("id") int id) {
		this.developerDao.delete(id);
		// return Response.ok("delete").build();
	}

	@DELETE
	@Path("/deleteall")
	public Response deleteAllDevolper() {
		this.developerDao.deleteAll();
		return Response.ok("deleteAll").build();
	}

}
