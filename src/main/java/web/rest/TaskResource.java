/**
 * 
 */
package web.rest;

import java.util.List;

import io.swagger.annotations.Api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.TaskDao;
import domain.Task;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
@Path("/task")
@Api(value = "/task", description = "Task resource")
public class TaskResource {
	
	private TaskDao taskDao;

	/**
	 * 
	 */
	public TaskResource() {
		this.taskDao = new TaskDao();
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTask(Task d) {
		this.taskDao.insert(d);
		return Response.ok("insert").build();
	}

	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTask(Task d) {
		this.taskDao.update(d);
		return Response.ok("update").build();
	}

	@GET
	@Path("/findall")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Task> getAllTask() {
		return this.taskDao.findAll();
	}

	@GET
	@Path("/find/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Task getTaskById(@PathParam("id") int id) {
		return this.taskDao.findById(id);
	}

	@DELETE
	@Path("/delete/{id}")
	public void deleteTask(@PathParam("id") int id) {
		this.taskDao.delete(id);
	}

	@DELETE
	@Path("/deleteall")
	public Response deleteAllTask() {
		this.taskDao.deleteAll();
		return Response.ok("deleteAll").build();
	}

}
