package jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import domain.*;

public class JpaTest {

	private EntityManager manager;

	public JpaTest(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("dev");
		EntityManager manager = factory.createEntityManager();

		JpaTest test = new JpaTest(manager);

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			/*
			 * Task tache = new Task(); tache.setNom("ToDo");
			 * 
			 * Developer dev1 = new Developer(); dev1.setNom("Rida");
			 * 
			 * tache.setDeveloper(dev1);
			 * 
			 * Sprint sprint = new Sprint(); sp.setNom("First");
			 * 
			 * tache.setSprint(sprint); tache.setBacklog(backlog);
			 * 
			 * manager.persist(tache); manager.persist(sprint);
			 */
			test.createDataBase();
			// test.createDeveloper();
			// test.removeDeveloper();

		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();

		test.getDeveloperById();
		test.getDevelopersCriteria();
		test.listDeveloper();

		manager.close();
		factory.close();

		System.out.println("..The END");
	}

	private void getDeveloperById() {
		List<String> resultList = manager.createQuery(
				"Select nom From Developer a", String.class).getResultList();
		System.out.println("Developer's id:" + resultList.size());
		for (String next : resultList) {
			System.out.println("next employee: " + next);
		}
	}

	private void createDataBase() {
		Task tache = new Task();
		tache.setNom("ToDo");

		Developer dev1 = new Developer();
		dev1.setNom("Rida");

		tache.setDeveloper(dev1);

		Sprint sp = new Sprint();
		sp.setNom("First");

		tache.setSprint(sp);
		
		manager.persist(dev1);
		manager.persist(tache);
		manager.persist(sp);
	}

	@SuppressWarnings("unused")
	private void removeDeveloper() {
		Developer dev = manager.find(Developer.class, 49);
		manager.remove(dev);
	}

	// add data inheritance developer
	@SuppressWarnings("unused")
	private void createDeveloper() {

		Developer dev1 = new Developer();
		dev1.setNom("Salma");
		
		manager.persist(dev1);

		Developer dev2 = new Developer();
		dev2.setNom("Zakaria");

		manager.persist(dev2);
	}

	// dispaly list developer "criteria"
	private void getDevelopersCriteria() {
		CriteriaBuilder cb = manager.getCriteriaBuilder();

		CriteriaQuery<Developer> c = cb.createQuery(Developer.class);
		@SuppressWarnings("unused")
		Root<Developer> developers = c.from(Developer.class);
		Query query = manager.createQuery(c);
		@SuppressWarnings("unchecked")
		List<Developer> result = (List<Developer>) query.getResultList();
		System.out.println("Developers : ");
		for (Developer dev : result) {
			System.out.println(dev);
		}
	}

	// dispaly list developer "namedquery"
	private void listDeveloper() {
		TypedQuery<Developer> query = manager.createNamedQuery(
				"Developer.findAll", Developer.class);
		List<Developer> result = query.getResultList();
		System.out.println("Developer : ");
		for (Developer dev : result) {
			System.out.println(dev.getNom());
		}
	}
}