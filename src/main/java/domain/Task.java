package domain;

import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Task
 * 
 */
@Entity
public class Task {

	private Long id;
	private String nom;
	private String description;
	private int priorite;
	private String etat;
	private Sprint sprint;

	public Task() {
	}

	public Task(Long id, String nom, String description, int priorite,
			String etat, Developer developer) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.priorite = priorite;
		this.etat = etat;
		this.developer = developer;
	}

	public Task(String nom, String desc, int pr, String etat) {
		super();
		this.nom = nom;
		this.description = desc;
		this.priorite = pr;
		this.etat = etat;
	}

	@ManyToOne
	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	private Developer developer;

	@ManyToOne
	public Developer getDeveloper() {
		return developer;
	}

	public void setDeveloper(Developer developer) {
		this.developer = developer;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPriorite() {
		return this.priorite;
	}

	public void setPriorite(int priorite) {
		this.priorite = priorite;
	}

	public String getEtat() {
		return this.etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

}
