package domain;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Entity implementation class for Entity: Developer
 * 
 */
@Entity
@Inheritance
@DiscriminatorColumn(name = "type")
@DiscriminatorValue("Developer")
@NamedQueries({
		@NamedQuery(name = "Developer.findAll", query = "SELECT d FROM Developer d"),
		@NamedQuery(name = "Developer.findByName", query = "SELECT d FROM Developer d WHERE d.nom = :nom"),
		@NamedQuery(name = "Developer.delete", query = "DELETE FROM Developer d WHERE d.nom = :nom"), })
public class Developer {

	private Long id;
	private String nom;

	private List<Task> Tasks = new ArrayList<Task>();

	public Developer(String nom) {
		super();
		this.nom = nom;
	}

	public Developer() {
	}

	public List<Task> Tasks() {
		return Tasks;
	}

	public void setTasks(List<Task> Tasks) {
		this.Tasks = Tasks;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return this.nom;
	}

	@OneToMany(mappedBy = "developer", cascade = CascadeType.PERSIST)
	@JsonIgnore
	public List<Task> getTasks() {
		return Tasks;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
